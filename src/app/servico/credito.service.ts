
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { HttpUtil } from "./httpUtil";
import { Observable } from 'rxjs/Observable';
import { Credito } from "../objeto/credito";


@Injectable()
export class CreditoService {

    URL = "http://localhost:8090/credito"; 

 constructor(
        private httpUtil: HttpUtil
    ) { }

    save(credito: Credito): Observable<any> {
        return this.httpUtil.post(this.URL + "/save", credito);
    }

    remove(id):  Observable<any> {
        return this.httpUtil.delete(this.URL + '/' + id);
    }

    listAll(): Observable<any> {
        return this.httpUtil.get(this.URL+ "/findAll");
    }

    listarRiscos(): Observable<any> {
        return this.httpUtil.get(this.URL+ "/listarRiscos");
    }
}