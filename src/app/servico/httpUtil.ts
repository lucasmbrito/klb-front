import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable()
export class HttpUtil {

  constructor(private http: HttpClient) {
  }


  public get(url: string, params?: any) {
    return this.http.get(url, this.getRequestOptionsGetDefault(params))
    .map((res: any) => {
      return res;
    })
    .catch((e: any) => {
      alert('erro ' + e);
      return Observable.throw(e);
    });

  }

  public delete(url: string, params?: any) {
    return this.http.delete(url, this.getRequestOptionsGetDefault(params))
    .map((res: any) => {
      return res;
    })
    .catch((e: any) => {
      alert('erro ' + e);
      return Observable.throw(e);
    });

  }

  public post(url: string, body: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };

    const jsonBody =  JSON.stringify(body);
    return this.http.post(url, jsonBody , httpOptions)
    .map((res: any ) => {
      return res;
    }).catch((e: any) => {
        alert('erro ' + e);
      return Observable.throw(e);
    });
  }

  public    getRequestOptionsPostDefault() {
    const httpHeader = new HttpHeaders();
    httpHeader.append('Content-Type', 'application/json');
    const options = {
      headers: new HttpHeaders()
    }
    options.headers = httpHeader;
    return options;
  }

  public getRequestOptionsGetDefault(params?: any) {
    const options = {
      params: new HttpParams()
    }
    options.params = params;
    return options;
  }


  public put(url: string, body: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };

    return this.http.put(url, JSON.stringify(body), httpOptions)
    .map((res: any ) => {
      return res;
    }).catch((e: any) => {
        alert('erro ' + e);
      return Observable.throw(e);
    });
  }

  

}
