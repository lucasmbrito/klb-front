export class Credito {
    id: number;
    nome: string;
    limiteCredito: number;
    tipoRisco: string;
    taxaJuros: number;
}