import { CreditoService } from './../servico/credito.service';
import { Credito } from './../objeto/credito';
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import { Alert } from 'selenium-webdriver';

declare var $: any;

@Component({
  selector: 'app-credito',
  templateUrl: './credito.component.html',
  styleUrls: ['./credito.component.css']
})
export class CreditoComponent implements OnInit {

  formulario: FormGroup;
  riscos: any[] = ['A', 'B', 'C'];
  creditos: Credito[] = [];


  constructor(
    private formBuilder: FormBuilder,
    private creditoService: CreditoService
) { }

  ngOnInit() {
    this.listAll();
    this.formulario = this.formBuilder.group({
      id: [null],
      nome: [null, Validators.required],
      limiteCredito: [null, Validators.required],
      tipoRisco: [this.riscos, Validators.required],
      taxaJuros: [null]

    });
  }

  zerarFormulario() {
    this.formulario.reset();
  }

  editar(credito: Credito) {
    console.log(credito);
    this.formulario = this.formBuilder.group({
      id: [credito.id],
      nome: [credito.nome],
      limiteCredito: [credito.limiteCredito],
      tipoRisco: [credito.tipoRisco],
      taxaJuros: [credito.taxaJuros]
    });    
  }

  excluir(credito: Credito) {
     this.creditoService.remove(credito.id).subscribe((r) => {
        this.listAll();
    }, err => {
        console.log(err);
        
    });
  }

  listAll(){
    this.creditoService.listAll().subscribe(
      r => this.creditos = r
    );    
  }

  save() {
    if(!this.formulario.invalid){
      this.creditoService.save(this.formulario.value).subscribe(r => {
        this.listAll();
        this.closeModelEmpresa();
      });
    }else{
      alert('Por favor preencher todos os campos');

    }
  }

  openModelEmpresa() {
    $('#myModal').modal('show');
  }

  closeModelEmpresa() {
    $('#myModal').modal('hide');
  }

}
